package com.sydneytester.Base;
 
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BasePage {

	public WebDriver driver;
	public ExtentTest test;

	public BasePage(WebDriver driver,ExtentTest test) {
		this.driver = driver;
		this.test=test;
	}



	 	public void wait(int timeTOWait)
	{
		try {
			Thread.sleep(timeTOWait*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


 

	public boolean isElementPresent(List<WebElement> elements){
		if(elements.size()==0){
			return false;	
		}
		else
			return true;
	}

 
	/*****************************Reporting********************************/

	public void reportPass(String msg){
		test.log(LogStatus.PASS, msg);
	}

	public void reportFailure(String msg){
		test.log(LogStatus.FAIL, msg);
		takeScreenShot();
		Assert.fail(msg);
	}

	public void takeScreenShot(){
		// fileName of the screenshot
		Date d=new Date();
		String screenshotFile=d.toString().replace(":", "_").replace(" ", "_")+".png";
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+"//screenshots//"+screenshotFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
		//put screenshot file in reports
		test.log(LogStatus.INFO,"Screenshot-> "+ test.addScreenCapture(System.getProperty("user.dir")+"//screenshots//"+screenshotFile));

	}
	
	 
	
	
}