package com.sydneytester.Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.sSydneyTesters.Pages.HomePage;
import com.sSydneyTesters.Pages.SydneyTestersCarInsurancePage;
import com.sSydneyTesters.Pages.BuyInsurncePage;

import com.relevantcodes.extentreports.ExtentTest;

public class sSydneyTesters_Pages {

	WebDriver driver;
	ExtentTest test;
	
	public sSydneyTesters_Pages(WebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test=test;
	}



	SydneyTestersCarInsurancePage insurencePage;
	public SydneyTestersCarInsurancePage insurencePage (){

		insurencePage = new SydneyTestersCarInsurancePage(driver, test);
		PageFactory.initElements(driver, insurencePage);
		return insurencePage;
	}


	HomePage homePage;
	public HomePage Home_Page (){

		homePage = new HomePage(driver, test);
		PageFactory.initElements(driver, homePage);
		return homePage;
	}


	 

	BuyInsurncePage buyInsurencePage;
	public BuyInsurncePage BuyInsurncePage (){

		buyInsurencePage = new BuyInsurncePage(driver, test);
		PageFactory.initElements(driver, buyInsurencePage);
		return buyInsurencePage;
	}

	
	
	
}



