package com.sydneytester.Base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sSydneyTesters.Utility.ExtentManager;
import com.sSydneyTesters.Utility.WebdriverListner;


public class BaseTest {

	public static Properties pro ;
	public WebDriver web_driver;
	public EventFiringWebDriver driver;
	public ExtentTest test ;
	public sSydneyTesters_Pages SydneyTestersPages;
	public String methodname;

	public ExtentReports extent= ExtentManager.getInstance();
	public String suiteName=getClass().getSimpleName();
	
	public ITestContext testcontext;
	



	@BeforeSuite
	public void beforesuite(){
		init();

	}

	 

	@BeforeMethod
	public void intialize(Method method){

		
		methodname = method.getName();
		test= extent.startTest(methodname);

 		launchBrowser();
		SydneyTestersPages = new sSydneyTesters_Pages(driver, test);
	}	

	@AfterMethod
	public void close(){

		if (extent!=null) {
			extent.endTest(test);
			extent.flush();

		}
		if (driver!=null) {
			driver.quit();

		}

	}


	public  void init(){
		pro = new Properties();
		try {
			FileInputStream fis = new FileInputStream(new File(System.getProperty("user.dir")+"\\config.properties"));
			pro.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void launchBrowser(){
		if ( pro.getProperty("BROWSER_TYPE").equals("ff")) {

			test.log(LogStatus.INFO, "Launching FireFox Browser");
			web_driver = new FirefoxDriver();

		} else if(pro.getProperty("BROWSER_TYPE").equals("ie")) {
			test.log(LogStatus.INFO, "Launching Internet Explorer Browser");

			web_driver= new InternetExplorerDriver();
		}
		else if(pro.getProperty("BROWSER_TYPE").equals("chrome")) {
			test.log(LogStatus.INFO, "Launching Chrome Browser");

			web_driver= new ChromeDriver();

		}
		driver = new EventFiringWebDriver(web_driver);
		WebdriverListner list= new WebdriverListner(driver, test);
		driver.register(list);

		test.log(LogStatus.INFO, "Maximizing Browser");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait((Integer.parseInt(pro.getProperty("SYNC_TIMEOUT"))), TimeUnit.SECONDS);

		test.log(LogStatus.INFO, "Navigating to URL"+pro.getProperty("url"));
		driver.get(pro.getProperty("HOME_URL"));

	}


	 

}
