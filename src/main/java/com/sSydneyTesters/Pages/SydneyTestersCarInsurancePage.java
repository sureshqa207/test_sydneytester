package com.sSydneyTesters.Pages;

import java.util.List;

import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sydneytester.Base.BasePage;



public class SydneyTestersCarInsurancePage extends BasePage    {


	public SydneyTestersCarInsurancePage(WebDriver driver, ExtentTest test) {
		super(driver, test);
	}

	@FindBy(xpath="//h3[text()='Sydney Testers Car Insurance']")
	public List<WebElement> SydneyTestersCarInsurance_Header;


	@FindBy(id="make")
	public WebElement make_dropDown;

	@FindBy(id="year")
	public WebElement year_TextBox;

	@FindBy(id="age")
	public WebElement age_TextBox;

	@FindBy(id="male")
	public WebElement male_RadioButton;

	@FindBy(id="state")
	public WebElement state_dropDown;

	@FindBy(id="email")
	public WebElement email_TextBox;

	@FindBy(id="getquote")
	public WebElement getquote_btn;


	public void carInsurencePageDisplayed(){


		if (isElementPresent(SydneyTestersCarInsurance_Header)) {

			reportPass("Sydney Testers CarInsurance Screen Displayed");			  

		} else {


			reportFailure("Sydney Testers CarInsurance Screen not Displayed");

		}

	}





	public void GetQuote()
	{

		test.log(LogStatus.INFO, "Enter all details required for Insurance");

		make_dropDown.sendKeys("BMW");
		year_TextBox.sendKeys("1");
		age_TextBox.sendKeys("22");
		male_RadioButton.click();
		state_dropDown.sendKeys("Victoria");
		email_TextBox.sendKeys("test@gmail.com");
		 
		getquote_btn.click();

	}



}
