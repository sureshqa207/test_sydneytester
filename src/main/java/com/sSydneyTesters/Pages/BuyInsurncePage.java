package com.sSydneyTesters.Pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sydneytester.Base.BasePage;
import com.sydneytester.Base.BaseTest;



public class BuyInsurncePage extends BasePage    {


	public BuyInsurncePage(WebDriver driver, ExtentTest test) {
		super(driver, test);
	}

	@FindBy(id="payment")
	public List<WebElement> buyInsurence_link;


 	@FindBy(id="payment")
	public WebElement buyInsurence;


	public void buyInsurencePageDisplayed(){

	 
			if (isElementPresent(buyInsurence_link)) {

				reportPass("Sydney Testers Buy CarInsurance Screen Displayed");			  

			} else {


				reportFailure("Sydney Testers Buy CarInsurance Screen not Displayed");

			}

		}


	public void clickonBuyInsurence(){
		
		test.log(LogStatus.INFO, "CLick on Buy insurence Link");
		buyInsurence.click();
	}


	
	

	 



}
