package com.sSydneyTesters.Pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sydneytester.Base.BasePage;



public class HomePage extends BasePage    {


	public HomePage(WebDriver driver, ExtentTest test) {
		super(driver, test);
	}


	@FindBy(xpath="//h3[text()='Sydney Testers Insurance']")
	public List<WebElement> SydneyTestersInsuranceHeader;

	@FindBy(id="getcarquote")
	public WebElement getcarquote_link;
	
	@FindBy(id="getlifequote")
	public WebElement getlifequote_link;
	
	



	public void IsHomePageDisplayed()
	{
		
		if (isElementPresent(SydneyTestersInsuranceHeader)) {

			reportPass("Sydney Testers Home Page Displayed");			  

		} else {


			reportFailure("Sydney Testers CarInsurance Home Page not Displayed");

		}
	}

	public void ClickonGetlifequote(){
		
		test.log(LogStatus.INFO, "CLick on getlifequote Link");
		getlifequote_link.click();
	}
	

	
	public void Clickongetcarquote(){
		
		test.log(LogStatus.INFO, "CLick on getcarquote Link");
		getcarquote_link.click();
	}

}
