package com.sSydneyTesters.Utility;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class WebdriverListner implements WebDriverEventListener{

	WebDriver driver;
	ExtentTest test;
	WebElement element;
	public WebdriverListner(WebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test=test;
	}

	public String nameofElement(WebElement element) {

		String x=element.toString();
		int begindex= x.indexOf("->");
		String result = x.substring(begindex).replace("]", "");
		return result.toString();
	}

	public void afterChangeValueOf(WebElement element, WebDriver arg1) {
		((JavascriptExecutor)driver).executeScript("arguments[0].style.border='0px'", element);				

		test.log(LogStatus.INFO,"Entered text "+ " "+"'"+element.getAttribute("value")+"' on "+nameofElement(element));
	}

	public void afterClickOn(WebElement element, WebDriver arg1) {
		//((JavascriptExecutor)driver).executeScript("arguments[0].style.border='0px'", element);				

		test.log(LogStatus.INFO, "Clicked on "+nameofElement(element));
	}

	public void afterFindBy(By arg0, WebElement arg1, WebDriver arg2) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateBack(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateForward(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateRefresh(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateTo(String arg0, WebDriver arg1) {
		// TODO Auto-generated method stub

	}

	public void afterScript(String arg0, WebDriver arg1) {
		// TODO Auto-generated method stub

	}

	public void beforeChangeValueOf(WebElement element, WebDriver driver) {


		((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid red'", element);		

	}

	public void beforeClickOn(WebElement element, WebDriver driver) {
		//((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid red'", element);		
	}

	public void beforeFindBy(By arg0, WebElement arg1, WebDriver arg2) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateBack(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateForward(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateRefresh(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateTo(String arg0, WebDriver arg1) {
		// TODO Auto-generated method stub

	}

	public void beforeScript(String arg0, WebDriver arg1) {
		// TODO Auto-generated method stub

	}

	public void onException(Throwable arg0, WebDriver arg1) {
		// TODO Auto-generated method stub

	}

	public void afterChangeValueOf(WebElement arg0, WebDriver arg1, CharSequence[] arg2) {
		// TODO Auto-generated method stub

	}

	public void beforeChangeValueOf(WebElement arg0, WebDriver arg1, CharSequence[] arg2) {
		// TODO Auto-generated method stub

	}

}
