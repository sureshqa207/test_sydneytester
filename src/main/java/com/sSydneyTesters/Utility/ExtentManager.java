package com.sSydneyTesters.Utility;

import java.io.File;
import java.util.Date;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;

public class ExtentManager {


	private static ExtentReports extent;

	public static ExtentReports getInstance() {
		if (extent == null) {
			Date date=new Date();
			String fileName=date.toString().replace(":", "_").replace(" ", "_")+".html";
			String reportPath = System.getProperty("user.dir")+"\\Reports\\"+fileName;
			
			//System.out.println("file name" +fileName);
			//System.out.println("report path"+reportPath);

			extent = new ExtentReports(reportPath, true, DisplayOrder.NEWEST_FIRST,NetworkMode.OFFLINE);
			extent.loadConfig(new File(System.getProperty("user.dir")+"\\ReportsConfig.xml"));

		}
		return extent;
	}

}
